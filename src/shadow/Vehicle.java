package shadow;

public abstract class Vehicle {
		private String type;
		
		public Vehicle(String type) {
			this.type = type;
		}
		
		public abstract int run(); 
		
		public String toString()	{
			return this.type;
		}
	
}
