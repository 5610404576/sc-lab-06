package shadow;

public class Bike extends Vehicle {
		int speed = 30;
		
		public int speedField (){
			return speed;
		}
		
		public int speedLocal() {
			int speed = 40;
			return speed;
		}

		public Bike(String type) {
			super(type);
		}

		@Override
		public int run() {
			return 45;
		}
	

}
