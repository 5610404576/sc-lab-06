package shadow;

public class Car extends Vehicle {
	int speed = 120;
	
	public int speedField (){
		return speed;
	}
	
	public int speedLocal() {
		int speed = 130;
		return speed;
	}

	public Car(String type) {
		super(type);
	}

	@Override
	public int run() {
		return 140;
	}
}

