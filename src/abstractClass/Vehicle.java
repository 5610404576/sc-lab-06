package abstractClass;

public abstract class Vehicle {


	private String brand;

	public Vehicle(String brand){
		this.brand = brand;
	}
	
	public abstract String run();
	//public  String run();  ���������  abstract ���Դ syntax error 
	
	public String toString(){
		return this.brand;
	}
}
