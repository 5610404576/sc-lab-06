package abstractClass;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bike bike = new Bike("LA");
		Car car = new Car("Porsche");
		System.out.println(bike);
		System.out.println(car);
		
		ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();
		vehicles.add(bike);
		vehicles.add(car);
		
		for(Vehicle vehicle : vehicles){
			System.out.println(vehicle.run());
		}
	}

}
