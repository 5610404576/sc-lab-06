package abstractClass;

public class Car extends Vehicle{
		
	public Car (String brand){
		super(brand);
	}

	@Override
	public String run(){
		return "100 km/hr";
	}
}
