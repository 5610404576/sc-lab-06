package abstractClass;

public class Bike extends Vehicle{
	
	public Bike (String brand){
		super(brand);
	}

	@Override
	public String run(){
		return "20 km/hr";
	}
}
