
public class Bmw extends Car  {

	// 1. answer
	//public Bmw(){
	//	super();
	//}
	
	// 2. answer
	//public Bmw(String name){	
	//}
	
	//3. answer
	//public Bmw(){	
	//}
	
	//public Bmw(String name){  passing parameter = error
	//}
	
	//4. answer
	public Bmw(){
		String name = "BMW Series 6 640i";
		System.out.println(name);
	}
}
